<?php
  $partner_header = get_field('partner_header', 'option');
 ?>

<section class="footer-partner-section">
 <div class="wrap">
  <div class="partner-container">
    <h2><?php echo $partner_header; ?></h2>
    <div class="partner-block">
      <?php if ( have_rows( 'partner_callout_row', 'option' )) : ?>
        <?php while ( have_rows( 'partner_callout_row', 'option' )) : the_row();
          $partner_link = get_sub_field('partner_link', 'option');
          $partner_logo = get_sub_field('partner_logo', 'option');
        ?>
        <div class="logo">
        <?php if ( $partner_link ) : ?>
          <a
          href="<?php echo esc_url( $partner_link['url'] ); ?>"
          target="<?php echo esc_attr( $partner_link['target'] ); ?>">
             <img src="<?php echo $partner_logo['url']; ?>" alt="<?php echo $partner_logo['alt']; ?>" />
           </a>
         <?php else : ?>
            <img src="<?php echo $partner_logo['url']; ?>" alt="<?php echo $partner_logo['alt']; ?>" />
         <?php endif; ?>
        </div>

        <?php endwhile; ?>
      <?php endif; ?>
    </div>
  </div>
 </div>
</section>
