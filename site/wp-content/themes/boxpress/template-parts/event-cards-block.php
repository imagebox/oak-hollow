<?php
/**
 * Displays 4 events
 *
 * @package boxpress
 */

?>
<section>
	<?php if ( function_exists( 'tribe_get_events' ) ) : ?>
		<div class="wrap">
			<div class="events-block flex-wrap">
			<?php
			$args = array(
				'post_type'      => 'tribe_events',
				'posts_per_page' => 4,
			);

			$eposts = new WP_Query( $args );

			if ( $eposts->have_posts() ) :
				while ( $eposts->have_posts() ) :
					$eposts->the_post();
					?>
					<div class="events-block--event">
						<div class="events-block--event-date"><?php echo wp_kses_post( tribe_events_event_schedule_details() ); ?></div>
						<div class="events-block--event-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></div>
						<div class="events-block--event-text"><?php the_excerpt(); ?></div>
					</div>
					<?php
				endwhile;
			else :
				echo '<div class="events-block--no-events">There are no events</div>';
			endif;
			wp_reset_postdata();
			?>
			</div>
		</div>
		<?php
	else :
		echo '<div class="events-block--no-plugin">Please install The Events Calendar plugin OR hide this div by placing  .events-block--no-plugin {display:none;}  in your CSS</div>';
	endif;
	?>
</section>
