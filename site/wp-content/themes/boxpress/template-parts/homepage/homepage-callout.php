<?php
  $home_callout_header = get_field('home_callout_header');
  $home_callout_text = get_field('home_callout_text');
  $home_callout_link = get_field('home_callout_link');
?>

<section class="callout-section section">
  <div class="wrap">
    <div class="button-block">

      <?php if ( $home_callout_header ) : ?>
        <h2 class="callout-header"><?php echo $home_callout_header; ?></h2>
      <?php endif; ?>

      <div class="callout-body">
        <div class="button-callout-content">
          <?php if ( $home_callout_text ) : ?>
            <p><?php echo $home_callout_text; ?></p>
          <?php endif; ?>

      <?php if ( $home_callout_link ) : ?>
          <?php
            $home_callout_link_target = ! empty( $home_callout_link['target'] ) ? $home_callout_link['target'] : '_self';
          ?>
            <a class="button"
              href="<?php echo esc_url( $home_callout_link['url'] ); ?>"
              target="<?php echo esc_attr( $home_callout_link_target ); ?>">
              <?php echo $home_callout_link['title']; ?>
            </a>
          <?php endif; ?>

        </div>
      </div>
    </div>
  </div>
</section>
