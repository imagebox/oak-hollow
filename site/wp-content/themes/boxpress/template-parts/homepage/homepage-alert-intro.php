<?php
  $home_split_heading = get_field('home_split_heading');
  $home_split_text = get_field('home_split_text');
 ?>


<section class="section home-alert-split-section">
 <div class="wrap">
 <div class="split-box">
    <div class="l-two-thirds">
     <div class="l-one-third-col">
        <div class="content">
          <h2><?php echo $home_split_heading; ?></h2>
        </div>
     </div>
      <div class="l-two-third-col">
        <div class="content">
          <?php if ( $home_split_text ) : ?>
            <p><?php echo $home_split_text; ?></p>
          <?php endif; ?>
        </div>
     </div>
   </div>
 </div>
</div>
</section>
