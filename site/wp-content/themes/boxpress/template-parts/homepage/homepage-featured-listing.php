<section class="section home-featured-listing-section">

  <?php
     $home_featured_listing_heading = get_field('home_featured_listing_heading');
     $home_featured_listing_text = get_field('home_featured_listing_text');
     $home_featured_listing_link = get_field('home_featured_listing_link');
    ?>


    <div class="wrap wrap--limited">
      <div class="header">
        <h2><?php echo $home_featured_listing_heading; ?></h2>
        <p><?php echo $home_featured_listing_text; ?></p>
      </div>
    </div>

    <div class="wrap">
      <div class="l-grid l-grid--four-col">
        <?php if ( have_rows( 'homepage_featured_listing' )) : ?>
          <?php while ( have_rows( 'homepage_featured_listing' )) : the_row();
              $home_featured_cards_listing_thumb = get_sub_field('home_featured_cards_listing_thumb');
              $home_featured_cards_listing_link = get_sub_field('home_featured_cards_listing_link');
            ?>
              <div class="l-grid-item">
                <div class="card">
                  <?php if ( $home_featured_cards_listing_link ) : ?>
                      <?php
                        $home_featured_cards_listing_link_target = ! empty( $home_callout_link['target'] ) ? $home_callout_link['target'] : '_self';
                      ?>
                    <a
                      href="<?php echo esc_url( $home_featured_cards_listing_link['url'] ); ?>"
                      target="<?php echo esc_attr( $home_featured_cards_listing_link_target ); ?>">
                    <div class="card-header">
                      <?php if ( $home_featured_cards_listing_thumb ) : ?>
                        <img
                          src="<?php echo esc_url( $home_featured_cards_listing_thumb['url'] ); ?>"
                          width="<?php echo esc_attr( $home_featured_cards_listing_thumb['width'] ); ?>"
                          height="<?php echo esc_attr( $home_featured_cards_listing_thumb['height'] ); ?>"
                          alt="<?php echo esc_attr( $home_featured_cards_listing_thumb['alt'] ); ?>">
                      <?php endif; ?>
                    </div>
                    <div class="card-footer">
                      <span><?php echo $home_featured_cards_listing_link['title']; ?></span>
                    </div>
                  </a>
                <?php endif; ?>
                </div>

              </div>
            <?php endwhile; ?>
          <?php endif; ?>
        </div>

        <div class="button-block">
          <?php if ( $home_featured_cards_listing_link ) : ?>
              <?php
                $home_featured_listing_link_target = ! empty( $home_featured_listing_link['target'] ) ? $home_featured_listing_link['target'] : '_self';
              ?>
          <a class="button"
            href="<?php echo esc_url( $home_featured_listing_link['url'] ); ?>"
            target="<?php echo esc_attr( $home_featured_listing_link_target ); ?>">
            <?php echo $home_featured_listing_link['title']; ?>
          </a>
        <?php endif; ?>
        </div>
    </div>
</section>
