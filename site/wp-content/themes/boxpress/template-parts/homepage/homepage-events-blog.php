<section class="section home-events-blog-section">


  <div class="wrap">
  <div class="l-grid l-grid--two-col">
   <div class="l-grid-item">
     <?php

       wp_reset_query();

       global $post;

       // Retrieve the next 5 upcoming events
       $events = tribe_get_events( [ 'posts_per_page' => 2, 'start_date' => 'now', ] );
       $feature_image = get_the_post_thumbnail_url( get_the_ID() );

     ?>

         <div class="l-grid l-grid--two-col events-grid">
         <h2>Upcoming Events</h2>
         <?php foreach ( $events as $post ) {
            setup_postdata( $post );

            // This time, let's throw in an event-specific
            // template tag to show the date after the title!

            echo '<div class="l-grid-item">';
            echo '<div class="card">';
            echo '<a href="' . get_permalink($post) . '">';
            echo '<div class="card-header">';
            echo '<h3>' . $post->post_title . '</h3>';
            echo '<p class="date">' . tribe_get_start_date( $post ) . '<p>';
            echo '</div>';
            echo '<div class="card-footer">';
            echo '<div class="button-four">' . "See Details" . '</div>';
            echo '</div>';
            echo '</a>';
            echo '</div>';
            echo '</div>';

         } ?>
         </div>


      <?php wp_reset_postdata(); ?>

   </div>


 <div class="l-grid-item blog-grid">
   <h2>Instagram</h2>
    <div class="l-grid l-grid--two-col">
      <div class="home-insta">
        <?php the_field('instagram_feed') ?>
      </div>
    </div>
 </div>

  <!-- <?php // Latest Posts Example ?>
  <?php
    $home_post_query_args = array(
      'post_type' => 'post',
      'posts_per_page' => 2
    );
    $home_post_query = new WP_Query( $home_post_query_args );
  ?>
  <?php // if ( $home_post_query->have_posts() ) : ?>

     <div class="l-grid-item blog-grid">
      <h2>Blog</h2>
        <div class="l-grid l-grid--two-col">

          <?php // while ( $home_post_query->have_posts() ) : $home_post_query->the_post(); ?>
            <div class="l-grid-item">
              <div class="card">
                <a href="<?php // echo get_the_permalink(); ?>">
                <div class="card-header">
                  <h3><?php // the_title(); ?></h3>
                  <p><?php // the_date(); ?></p>
                  <p><?php // the_excerpt(); ?></p>
                </div>
                <div class="card-footer">
                  <div class="button-four"><?php // _e('See Details', 'boxpress'); ?></div>
                </div>
                </a>
              </div>
            </div>
          <?php // endwhile; ?>
        </div>
      </div>
     </div>
   </div>

    <?php // wp_reset_postdata(); ?>
  <?php // endif; ?> -->

</section>
