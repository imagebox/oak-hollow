<section class="hero-slider">
  <?php
  /**
   * Displays the Slideshow layout
   *
   * @package boxpress
   */
  ?>
  <?php if ( have_rows( 'home-carousel' )) : ?>

    <div class="home-carousel">
      <div class="js-carousel owl-carousel owl-theme">

        <?php while ( have_rows( 'home-carousel' )) : the_row(); ?>
          <?php
            $slide_copy  = get_sub_field( 'slide_copy' );
            $slide_heading  = get_sub_field( 'slide_heading' );
            $slide_subhead  = get_sub_field( 'slide_subhead' );
            $slide_bkg      = get_sub_field( 'slide_bkg' );
          ?>

          <div class="carousel-slide">

            <?php if ( $slide_bkg ) : ?>
              <?php
                $image_size = 'home_slideshow';
              ?>
              <img class="slide-bkg" draggable="false" aria-hidden="true"
                src="<?php echo $slide_bkg['sizes'][ $image_size ]; ?>"
                width="<?php echo $slide_bkg['sizes'][ $image_size . '-width' ]; ?>"
                height="<?php echo $slide_bkg['sizes'][ $image_size . '-height' ]; ?>"
                alt="">
            <?php endif; ?>

            <div class="slide-wrap">
              <div class="slide-content">

              <?php echo $slide_copy; ?>


              </div>
            </div>
          </div>

        <?php endwhile; ?>

      </div>
    </div>

  <?php endif; ?>

</section>
