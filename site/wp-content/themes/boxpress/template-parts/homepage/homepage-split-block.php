<?php
/**
 * Displays the slipt content / image layout
 *
 * @package boxpress
 */

$home_split_heading_2    = get_field( 'home_split_heading_2' );
$home_split_content    = get_field( 'home_split_content' );
$home_split_image      = get_field( 'home_split_image' );
$home_split_bkg        = get_field( 'home_split_background' );

$split_image_size = 'block_half_width';
?>
<section class="split-block-layout split-block-section">
  <div class="split-block-col split-photo">
    <?php if ( $home_split_heading_2 ) : ?>
      <h2><?php echo $home_split_heading_2; ?></h2>
    <?php endif; ?>

    <?php if ( $home_split_image ) : ?>
      <img
        src="<?php echo esc_url( $home_split_image['url'] ); ?>"
        width="<?php echo esc_attr( $home_split_image['width'] ); ?>"
        height="<?php echo esc_attr( $home_split_image['height'] ); ?>"
        alt="<?php echo esc_attr( $home_split_image['alt'] ); ?>">
    <?php endif; ?>

  </div>
  <div class="split-block-col split-content">
    <div class="split-block-content">
      <div class="split-block-content-inner-wrap">
        <?php echo $home_split_content; ?>
      </div>
    </div>
  </div>
</section>
