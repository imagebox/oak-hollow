<?php
/**
 *  Template Name: Areas
 *
 * @package boxpress
 */


$area_content_block      = get_field( 'area_content_block' );
$area_map_photo_thumb      = get_field( 'area_map_photo_thumb' );
$area_map_photo      = get_field( 'area_map_photo' );
$area_content_block_bottom      = get_field( 'area_content_block_bottom' );
$area_content_bkg        = get_sub_field( 'area_content_block_background' );
$area_content_bkg_image  = get_sub_field( 'area_content_block_background_image' );
$area_content_bkg_size   = 'block_full_width';

$child_pages_list = query_for_child_page_list();

get_header(); ?>

<?php require_once('template-parts/banners/banner--page.php'); ?>

  <section class="fullwidth-layout map-section section <?php echo $content_bkg; ?>">
    <div class="wrap <?php
        if ( ! $child_pages_list ) {
          echo ' wrap--limited ';
        }
      ?>">
      <div class="<?php
          if ( $child_pages_list ) {
            echo 'l-sidebar';
          }
        ?>">
        <div class="l-main-col">
          <div class="area-section">
            <?php
            $media_headline = get_field( 'media_headline', get_the_ID() );
            $page_title = ( ! empty( $media_headline )) ? $media_headline : get_the_title( get_the_ID() );
            ?>
            <div class="page-content">
              <?php echo $area_content_block; ?>
            </div>
            <div class="map">
              <div class="map-upload">
                <?php if ( $area_map_photo_thumb ) : ?>
                <a href="#map" class="open-map">
                    <img class="map-thumb"
                    src="<?php echo esc_url( $area_map_photo_thumb['url'] ); ?>"
                    width="<?php echo esc_attr( $area_map_photo_thumb['width'] ); ?>"
                    height="<?php echo esc_attr( $area_map_photo_thumb['height'] ); ?>"
                    alt="<?php echo esc_attr( $area_map_photo_thumb['alt'] ); ?>">
                    <svg class="site-logo" width="64" height="64" focusable="false">
                      <use href="#enlarge-icon"/>
                    </svg>
                </a>
              <?php endif; ?>
              </div>
              <div id="map" class="mfp-hide popup">
                  <!-- Modal content -->
                  <div class="modal-content">
                    <img class="map-popup"
                    src="<?php echo esc_url( $area_map_photo['url'] ); ?>"
                    width="<?php echo esc_attr( $area_map_photo['width'] ); ?>"
                    height="<?php echo esc_attr( $area_map_photo['height'] ); ?>"
                    alt="<?php echo esc_attr( $area_map_photo['alt'] ); ?>">
                  </div>
              </div>
            </div>
            <div class="page-content">
              <?php echo $area_content_block_bottom; ?>
            </div>
          </div>
          <div class="l-aside-col-r">
            <div class="area-sidebar-container">
              <?php
              $area_sidebar_text = get_field('area_sidebar_text');
              $map_sidebar_header = get_field('map_sidebar_header');
              $direction_button = get_field('direction_button');
              ?>

              <div class="area-sidebar-text">
                <div class="page-content">
                  <?php echo $area_sidebar_text; ?>
                </div>
              </div>

                <div class="area-sidebar-map">
                  <div class="page-content">
                    <h3><?php echo $map_sidebar_header; ?></h3>
                    <div class="map">
                      <?php
                        $location = get_field('location');
                        if( $location ): ?>
                        <div class="acf-map" data-zoom="16">
                          <div class="marker" data-lat="<?php echo esc_attr($location['lat']); ?>" data-lng="<?php echo esc_attr($location['lng']); ?>"></div>
                        </div>
                        <?php endif; ?>
                     </div>

                    <?php if ( $direction_button ) : ?>
                      <?php
                      $direction_button_target = ! empty( $direction_button['target'] ) ? $direction_button['target'] : '_self';
                      ?>
                      <a class="button"
                      href="<?php echo esc_url( $direction_button['url'] ); ?>"
                      target="<?php echo esc_attr( $direction_button ); ?>">
                      <?php echo $direction_button['title']; ?>
                    </a>
                  <?php endif; ?>
                  </div>
                </div>

            </div>
          </div>
        </div>



        <?php if ( $child_pages_list ) : ?>
          <div class="l-aside-col">
            <?php get_sidebar(); ?>
          </div>
        <?php endif; ?>

      </div>
    </div>

    <?php if ( $content_bkg_image && $content_bkg === 'background-image' ) : ?>
      <img class="fullwidth-layout-bkg" draggable="false" aria-hidden="true"
        src="<?php echo esc_url( $content_bkg_image['url'] ); ?>"
        width="<?php echo esc_attr( $content_bkg_image['width'] ); ?>"
        height="<?php echo esc_attr( $content_bkg_image['height'] ); ?>"
        alt="">
    <?php endif; ?>

    <?php
      $areas_callout_header = get_field('areas_callout_header');
      $areas_callout_text = get_field('areas_callout_text');
      $areas_callout_link = get_field('areas_callout_link');
    ?>
  </section>
  <?php if ( $areas_callout_header ) : ?>
    <section class="callout-section section">
      <div class="wrap">
        <div class="button-block">

          <?php if ( $areas_callout_header ) : ?>
            <h2 class="callout-header"><?php echo $areas_callout_header; ?></h2>
          <?php endif; ?>

          <div class="callout-body">
            <div class="button-callout-content">
              <?php if ( $areas_callout_text ) : ?>
                <p><?php echo $areas_callout_text; ?></p>
              <?php endif; ?>

              <?php if ( $areas_callout_link ) : ?>
                <a class="button"
                  href="<?php echo esc_url( $areas_callout_link['url'] ); ?>"
                  target="<?php echo esc_attr( $areas_callout_link ); ?>">
                  <?php echo $areas_callout_link['title']; ?>
                </a>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
    </section>
  <?php endif; ?>



  <style type="text/css">
.acf-map {
    width: 100%;
    height: 400px;
    border: #ccc solid 1px;
    margin: 20px 0;
}

// Fixes potential theme css conflict.
.acf-map img {
   max-width: inherit !important;
}
</style>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDkibvwQGVHUcsKjRGElFglA1WdQFNdDII"></script>
<script type="text/javascript">
(function( $ ) {

/**
 * initMap
 *
 * Renders a Google Map onto the selected jQuery element
 *
 * @date    22/10/19
 * @since   5.8.6
 *
 * @param   jQuery $el The jQuery element.
 * @return  object The map instance.
 */
function initMap( $el ) {

    // Find marker elements within map.
    var $markers = $el.find('.marker');

    // Create gerenic map.
    var mapArgs = {
        zoom        : $el.data('zoom') || 16,
        mapTypeId   : google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map( $el[0], mapArgs );

    // Add markers.
    map.markers = [];
    $markers.each(function(){
        initMarker( $(this), map );
    });

    // Center map based on markers.
    centerMap( map );

    // Return map instance.
    return map;
}

/**
 * initMarker
 *
 * Creates a marker for the given jQuery element and map.
 *
 * @date    22/10/19
 * @since   5.8.6
 *
 * @param   jQuery $el The jQuery element.
 * @param   object The map instance.
 * @return  object The marker instance.
 */
function initMarker( $marker, map ) {

    // Get position from marker.
    var lat = $marker.data('lat');
    var lng = $marker.data('lng');
    var latLng = {
        lat: parseFloat( lat ),
        lng: parseFloat( lng )
    };

    // Create marker instance.
    var marker = new google.maps.Marker({
        position : latLng,
        map: map
    });

    // Append to reference for later use.
    map.markers.push( marker );

    // If marker contains HTML, add it to an infoWindow.
    if( $marker.html() ){

        // Create info window.
        var infowindow = new google.maps.InfoWindow({
            content: $marker.html()
        });

        // Show info window when marker is clicked.
        google.maps.event.addListener(marker, 'click', function() {
            infowindow.open( map, marker );
        });
    }
}

/**
 * centerMap
 *
 * Centers the map showing all markers in view.
 *
 * @date    22/10/19
 * @since   5.8.6
 *
 * @param   object The map instance.
 * @return  void
 */
function centerMap( map ) {

    // Create map boundaries from all map markers.
    var bounds = new google.maps.LatLngBounds();
    map.markers.forEach(function( marker ){
        bounds.extend({
            lat: marker.position.lat(),
            lng: marker.position.lng()
        });
    });

    // Case: Single marker.
    if( map.markers.length == 1 ){
        map.setCenter( bounds.getCenter() );

    // Case: Multiple markers.
    } else{
        map.fitBounds( bounds );
    }
}

// Render maps on page load.
$(document).ready(function(){
    $('.acf-map').each(function(){
        var map = initMap( $(this) );
    });
});

})(jQuery);
</script>



<?php get_footer(); ?>
