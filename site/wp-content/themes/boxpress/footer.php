

<?php get_template_part( 'template-parts/global/homepage-partners' ); ?>
<footer class="section">
  <div class="wrap">
      <div class="footer-box">
       <div class="footer-inner-box">
         <div class="top-grid">
           <div class="l-two-thirds">
             <div class="l-one-third-col">
               <a href="#form" class="open-form">
                 Sign Up for Our Newsletter
               </a>
             </div>
             <div id="form" class="mfp-hide popup">
                 <!-- Modal content -->
                 <div class="modal-content form-modal">
                    <?php echo gravity_form(4, false, false, false, '', true, 12);  ?>
                 </div>
             </div>
            <div class="l-two-third-col">
               <?php include( get_template_directory() . '/template-parts/search-bar-footer.php'); ?>
             </div>
           </div>
         </div>
         <div class="bottom-grid">
           <div class="l-grid l-grid--five-col">

                   <?php if ( have_rows( 'site_offices', 'option' )) : ?>
                     <?php while ( have_rows( 'site_offices', 'option' )) : the_row(); ?>
                       <?php
                         $po_box = get_sub_field('po_box');
                         $po_city = get_sub_field('po_city');
                         $po_state = get_sub_field('po_state');
                         $po_zip = get_sub_field('po_zip');
                         $street_address = get_sub_field('street_address');
                         $address_line_2 = get_sub_field('address_line_2');
                         $city           = get_sub_field('city');
                         $state          = get_sub_field('state');
                         $zip            = get_sub_field('zip');
                         $country        = get_sub_field('country');
                         $secondary_address          = get_sub_field('secondary_address');
                         $phone_number   = get_sub_field('phone_number');
                         $email          = get_sub_field('email');
                         $full_street_address = $street_address;

                         if ( ! empty( $address_line_2 )) {
                           $full_street_address = "{$street_address}, {$address_line_2}";
                         }
                       ?>


                       <div class="l-grid-item">
                        <div class="po-box">
                          <p>
                            <?php if ( ! empty( $po_box )) : ?>
                              <span itemprop="pobox"><?php echo $po_box; ?></span><br>
                            <?php endif; ?>


                            <?php if ( ! empty( $po_city )) : ?>
                              <span itemprop="addressLocality"><?php echo $po_city; ?></span>,
                            <?php endif; ?>

                            <?php if ( ! empty( $po_state ) || ! empty( $po_zip )) : ?>
                              <span itemprop="addressRegion"><?php echo $po_state; ?> <?php echo $po_zip; ?></span>
                            <?php endif; ?>
                          </p>
                        </div>
                       </div>

                       <div class="l-grid-item">
                        <div class="street-address">
                          <p>
                            <?php if ( ! empty( $street_address )) : ?>
                              <span itemprop="streetAddress"><?php echo $full_street_address; ?></span><br>
                            <?php endif; ?>

                            <?php if ( ! empty( $city )) : ?>
                              <span itemprop="addressLocality"><?php echo $city; ?></span>,
                            <?php endif; ?>

                            <?php if ( ! empty( $state )) : ?>
                              <span itemprop="addressRegion"><?php echo $state; ?></span>
                            <?php endif; ?>

                            <?php if ( ! empty( $zip )) : ?>
                              <span itemprop="postalCode"><?php echo $zip; ?></span>
                            <?php endif; ?>

                            <?php if ( ! empty( $country )) : ?>
                              <span class="vh" itemprop="addressCountry"><?php echo $country; ?></span>
                            <?php endif; ?>
                          </p>
                        </div>
                       </div>

                       <div class="l-grid-item">
                         <div class="phone-box">
                           <?php if ( ! empty( $phone_number )) : ?>
                             <?php
                               // Strip hyphens & parenthesis for tel link
                               $tel_formatted = str_replace([ ".", "-", "–", "(", ")", " " ], '', $phone_number );
                             ?>
                             <p>
                               <span class="vh"><?php _e( 'Phone:', 'boxpress' ); ?></span>
                               <a href="tel:+1<?php echo $tel_formatted; ?>">
                                 <span itemprop="telephone"><?php echo $phone_number; ?></span>
                               </a>
                             </p>
                           <?php endif; ?>
                         </div>
                       </div>

                       <div class="l-grid-item">
                         <div class="email-box">
                           <?php if ( ! empty( $email )) : ?>
                             <p>
                               <span class="vh"><?php _e( 'Email:', 'boxpress' ); ?></span>
                               <a class="email" href="mailto:<?php echo $email; ?>">
                                 <span itemprop="email"><?php echo $email; ?></span>
                               </a>
                             </p>
                           <?php endif; ?>
                         </div>
                       </div>

                       <div class="l-grid-item">
                          <?php get_template_part( 'template-parts/global/social-nav' ); ?>
                       </div>

                     <?php endwhile; ?>
                   <?php endif; ?>

           </div>
         </div>
       </div>
       <div class="site-info">
         <div class="l-footer l-footer--3-cols">
           <div class="l-footer-item">
             <div class="site-copyright">
               <p>
                 <small>
                   <?php _e('Copyright', 'boxpress'); ?> &copy; <?php echo date('Y'); ?>
                   <?php
                     $company_name     = get_bloginfo( 'name', 'display' );
                     $alt_company_name = get_field( 'alternative_company_name', 'option' );

                     if ( ! empty( $alt_company_name )) {
                       $company_name = $alt_company_name;
                     }
                   ?>
                   <?php echo $company_name; ?>.
                   <?php _e('All rights reserved.', 'boxpress'); ?>
                 </small>
               </p>
             </div>
           </div>
           <?php // Footer Navigation ?>
           <?php if ( has_nav_menu( 'footer' )) : ?>
             <div class="l-footer-item">
               <nav class="navigation--footer"
                 aria-label="<?php _e( 'Footer Navigation', 'boxpress' ); ?>"
                 role="navigation">
                 <ul class="nav-list">
                   <?php
                     wp_nav_menu( array(
                       'theme_location'  => 'footer',
                       'items_wrap'      => '%3$s',
                       'container'       => false,
                       'walker'          => new Aria_Walker_Nav_Menu(),
                     ));
                   ?>
                 </ul>
               </nav>
             </div>
           <?php endif; ?>
           <div class="l-footer-item l-footer-item--pull-right">
             <div class="imagebox">
               <p>
                 <small>
                   <?php _e('Website by', 'boxpress'); ?>
                   <a href="https://imagebox.com" target="_blank">
                     <span class="vh">Imagebox</span>
                     <svg class="imagebox-logo-svg" width="81" height="23" focusable="false">
                       <use href="#imagebox-logo"/>
                     </svg>
                   </a>
                 </small>
               </p>
             </div>
           </div>
         </div>
       </div>
      </div>
  </div>

</footer>

<?php wp_footer(); ?>

<?php // Footer Tracking Codes ?>
<?php the_field( 'footer_tracking_codes', 'option' ); ?>

</body>
</html>
