(function ($) {
  'use strict';

  /**
   * Sticky Header
   * ===
   */
  
  var $site_header = $('#masthead');

  // Init Sticky Nav
  toggleStickyNav( $site_header );

  // Re-calculate sticky on scroll
  $(window).on( 'scroll', function () {
    toggleStickyNav( $site_header );
  });

  // Re-calculate sticky on window resize
  $(window).on( 'resize', function () {
    toggleStickyNav( $site_header );
  });


  /**
   * Toggle Sticky Class
   * ---
   */

  function toggleStickyNav( $el ) {
    if ( $el.is( ':visible' ) && $(window).scrollTop() > 40 ) {
      $el.addClass('minify-header');
    } else {
      $el.removeClass('minify-header');
    }
  }

})(jQuery);
