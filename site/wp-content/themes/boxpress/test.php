<?php
/**
 * Template Name: Events
 *
 * @package boxpress
 */

$event_category_terms = get_terms(array(
  'taxonomy' => 'tribe_events_cat',
  'hide_empty' => true,
));

$has_sidebar = false;

if ( $event_category_terms && ! is_wp_error( $event_category_terms ) ) {
  $has_sidebar = true;
}

?>
<?php get_header(); ?>

  <?php require_once( 'template-parts/banners/banner--events.php' ); ?>

  <section class="fullwidth-column section">
    <div class="wrap <?php if ( ! $has_sidebar ) { echo 'wrap--limited'; } ?>">

      <div class="<?php if ( $has_sidebar ) { echo 'l-sidebar'; } ?>">
        <div class="l-main-col">

          <?php while ( have_posts() ) : the_post(); ?>
            <?php get_template_part( 'template-parts/content/content', 'events-page' ); ?>
          <?php endwhile; ?>

          <div class="back-top back-top--article vh">
            <a href="#main"><?php _e('Back to Top', 'boxpress'); ?></a>
          </div>
        </div>

        <?php if ( $has_sidebar ) : ?>
          <div class="l-aside-col">
            <?php get_sidebar( 'events' ); ?>
          </div>
        <?php endif; ?>

      </div>

    </div>
  </section>

<?php get_footer(); ?>
