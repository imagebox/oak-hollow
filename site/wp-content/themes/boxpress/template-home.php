<?php
/**
 * Template Name: Homepage
 *
 * Page template to display the homepage.
 *
 * @package boxpress
 */
get_header(); ?>

  <article class="homepage">

    <?php // Hero ?>
    <?php get_template_part( 'template-parts/homepage/homepage-slideshow' ); ?>
    <?php get_template_part( 'template-parts/homepage/homepage-alert-box' ); ?>
    <?php get_template_part( 'template-parts/homepage/homepage-alert-intro' ); ?>
    <?php get_template_part( 'template-parts/homepage/homepage-callout' ); ?>
    <?php get_template_part( 'template-parts/homepage/homepage-featured-listing' ); ?>
    <?php get_template_part( 'template-parts/homepage/homepage-split-block' ); ?>
    <?php get_template_part( 'template-parts/homepage/homepage-events-blog' ); ?>

  </article>

<?php get_footer(); ?>
