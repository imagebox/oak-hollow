<?php
/**
 * Events Sidebar
 *
 * @package boxpress
 */
?>
<aside class="sidebar" role="complementary">

  <?php
    $event_category_terms = get_terms(array(
      'taxonomy' => 'tribe_events_cat',
      'hide_empty' => true,
    ));
  ?>
  <?php if ( $event_category_terms && ! is_wp_error( $event_category_terms )) : ?>
    <div class="sidebar-widget">
      <h4 class="widget-title"><?php _e( 'Categories', 'boxpress' ); ?></h4>
      <nav class="categories-widget">
        <ul>
          <li class="<?php
              if ( is_post_type_archive( 'tribe_events' )) {
                echo 'current-cat';
              }
            ?>">
            <a href="<?php echo esc_url( tribe_get_events_link() ); ?>">
              <?php _e( 'All Events', 'boxpress' ); ?>
            </a>
          </li>

          <?php foreach ( $event_category_terms as $term ) : ?>
            <li <?php
                if ( is_tax( 'tribe_events_cat', $term->term_id )) {
                  echo 'current-cat';
                }
              ?>>
              <a href="<?php echo esc_url( get_term_link( $term )) ?>"><?php echo $term->name; ?></a>
            </li>
          <?php endforeach; ?>

        </ul>
      </nav>
    </div>
  <?php endif; ?>

</aside>
